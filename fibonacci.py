#!/usr/bin/python3
import cgi

# Print header
print("Content-Type: text/html")
print()


# Define our variables
arguments = cgi.FieldStorage(keep_blank_values=1)
values = len(arguments.keys())
dictionary =  (arguments.value)


# First check: Do we have anyinput supplied?
if 'FieldStorage(None, None, [])' in str(arguments):
	print('No input found')
	exit(1)

# Second check: Eleminate everything like http://site.org/cgi-bin/script.py?n=10&n=30
for key in arguments.keys():
	variable = str(key)
	value = str(arguments.getvalue(variable))
	r = "<p>"+ variable + " " + value +"</p>\n" 
fields = str(r)

chars = []
if any((c in chars) for c in fields):
	print('Too much arguments')
	exit(1)

# Third check: ensure, that we have only one value given via GET
if int(values) != 1:
	print("Too much arguments")
	exit(1)

# Last check: Ensure, that only 'n' is the allowed literal
chars2 = 'n'

for i in arguments.keys():
	literal = (arguments[i].name)

if any((c not in chars2) for c in literal):
	print('Illegal form')
	exit(1)

# If the check is successfull, assign more handy names for our value pair
for i in arguments.keys():
	number = (arguments[i].value)
	constant = (arguments[i].name)

# Check if our value is integer
try:
	numberint = int(number)

except ValueError:
	print("Value not integer")
	exit(1)
	
# If n=0, we'll get a logical problem
if int(number) == 0:
	print("n is 0")
	exit(1)

# Calc!
def fib():
	a, b = 0, 1
	while True:            # First iteration:
		yield a            # yield 0 to start with and then
		a, b = b, a + b    # a will now be 1, and b will also be 1, (0 + 1)

# break after (n - 1) values
for index, fibonacci_number in enumerate(fib()):
	print('{f:3}'.format(f=fibonacci_number))
	if index == int(number) - 1:
		break

exit(0)

